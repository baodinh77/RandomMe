$(() => {
  $('#emailButton').click(() => {
    var templateEmail = $('#templateEmail').val();
    var templateEmailArray = templateEmail.split('@');
    var randomNumber = Math.floor(Math.random(100000000, 900000000) * 100000000);
    var newEmail = templateEmailArray[0] + randomNumber + '@' + templateEmailArray[1];
    $('#emailText').text(newEmail);
  });

  $('#phoneButton').click(() => {
    var areaCode = $('#areaCode').val();
    if(!areaCode) {
      areaCode = "949";
    }
    var randomNumber = Math.floor(Math.random(10000000, 90000000) * 10000000);
    var newPhone = areaCode + randomNumber;
    $('#phoneText').text(newPhone);
  });
});
