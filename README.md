RandomMe
===========

A random Email and Phone Generator

RandomMe allows you to generate random email base on template

#### Example

Email: If email template is: email@domain.com, it generates random email like email98313@domain.com

Phone: provide area code, it appends area code with 7 random digits. Default Area code: 949
